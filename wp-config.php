<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'easystreet_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Xi={E&R4PL).dI77e(~/s<#zJ0_?EF=9Xv2Ig#u]1pM/MV{9NGaWEC{C/4D:RpB');
define('SECURE_AUTH_KEY',  '> e<%uDxrL/j|$$e6O9wD3|r)nz.,PPD97!18^4!=ye-F*i,CrDhr?;=P6W^754+');
define('LOGGED_IN_KEY',    'fdjZrT2o.{&>t4e%m#~;2g_p7{S} bM@_v|wl*HkR.EJi5 8;tHe awBEGBZ>b7&');
define('NONCE_KEY',        '5<+fwcY=Ym#hKJ7=@T7Y|[z`Hcek7to_R!X$IXqgi*kk`WJa@nl,7.GwYtyUxdQv');
define('AUTH_SALT',        'J,9vp;_W~6e9[{)+$*a3<*Vb-~4y2v}kG!0xjfJ9*@4d^%r#,e5xT9Hb9Hf)_Bic');
define('SECURE_AUTH_SALT', '{ADH>S&Twx;e>asBTj:]-8;#<,^Pl]%<=hSlrd?*N}j#(51:@m+@1=(*&|S/j0Z|');
define('LOGGED_IN_SALT',   '%y-Vg/D7zYGk{}RJOu[&[-tGP y<SY 3p}K8xfHdssuzB,j`qdr;jmOaC[(Tco3M');
define('NONCE_SALT',       'YjSK6R*K1Z5*6EV)`Hy(0q2W5rE+$o<QmJZ DO;D=|d!gY)nf`4O/q5Xn2s9*t}(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
