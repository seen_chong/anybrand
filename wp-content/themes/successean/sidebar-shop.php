<!-- sidebar -->
<aside class="sidebar-shop" role="complementary">

	<div class="cat-list">
		<ul>
			<li>New Arrivals</li>
			<li>Summer Collection</li>
			<li>Deluxe Collection</li>
			<li>Casual</li>
			<li>Sandals</li>
			<li>Boots</li>
			<li>Dress</li>
			<li>Wide Calf</li>
			<li>Tuscany</li>
			<li>Sport</li>
			<li><span class="red">Sale</span></li>
		</ul>	
	</div>
</aside>
<!-- /sidebar -->
