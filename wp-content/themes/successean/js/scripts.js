(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		$('select#pa_color option:contains("choose an option")').text('select a color');

		$('select#pa_sizes option:contains("choose an option")').text('select a size');
		
		$('select#pa_width option:contains("choose an option")').text('select a width');
		
	});
	
	$(function () {
		$('a[href^="#"]').on('click', function(event) {
		    var target = $(this.getAttribute('href'));
		    if( target.length ) {
		        event.preventDefault();
		        $('html, body').stop().animate({
		            scrollTop: target.offset().top
		        }, 1000);
		    }
		});
	});
})(jQuery, this);
