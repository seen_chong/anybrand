			</div><!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="wrapper-94">
				<!-- copyright -->
					<div class="footer-container">
						<div class="footer-bar-left">
							<h4 class="copyright">
								Copyright &copy;  Anybrand Shoes <?php echo date('Y'); ?> &bull; Terms of Service &bull; Privacy Policy
							</h4> 
						</div>
						<div class="footer-bar-right">
							<div class="newsletterBarRight">
								<p id="footerCall">get notified</p>
								<?php echo do_shortcode('[contact-form-7 id="85" title="Newsletter Subscription"]'); ?>		
								<ul class="footerSocial">
									<a href="" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/img/footer-social.png">
									</a>
								</ul>			
							</div>
						</div>
					</div>
				<!-- /copyright -->
				</div>
			</footer>
			<!-- /footer -->
		
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->

	</body>
</html>
<script>
  $(document).ready(function(){
  	$("#sticker").sticky({topSpacing:45});
  });
    $(document).ready(function(){
  	$("#sticker1").sticky({topSpacing:110});
  });
</script>