<?php get_header(); ?>

	<main role="main">

		<section class="hero">
			<div class="wrapper-100">
				<div class="hero-banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/flower-charming.jpg)">
					<a href="/shop">
						<div class="hero-overlay">
							<h1><?php the_field('hero_banner_top_text'); ?></h1>
							<h3><?php the_field('hero_banner_bottom_text'); ?></h3>
						</div>
					</a>
					<a href="/shop" class="view-button"><button>shop new arrivals</button></a>
				</div>
			</div>
			
		</section>

		<section class="block-shop">
				<div class="left-block">
					<a href="/shop">
						<img src="<?php echo get_template_directory_uri(); ?>/img/casual-kicks.jpg" alt="casual-kicks" >
					</a>
					<a href="/shop" class="view-button"><button>shop casual</button></a>
				</div>
					<div class="right-block">
						<a href="/shop">
							<img src="<?php echo get_template_directory_uri(); ?>/img/dress-shoes.jpg" alt="dress-shoes">
						</a>
						<a href="/shop" class="view-button"><button>shop dress</button></a>
					</div>
				</div>
		</section>


		<section class="block-sale">
			<div class="wrapper-100">
				<div class="block-sale-banner">
					<a href="/shop">
						<img src="<?php echo get_template_directory_uri(); ?>/img/heels-shoes-purchase-models.jpg" alt="block-sale-banner" class="hero-banner">
						<div class="hero-overlay">
							<h1>Sale on Heels</h1>
							<h3>up to <span class="big-h3">30%</span> OFF</h3>
						</div>
					</a>
					<a href="/shop" class="view-button"><button>shop sale</button></a>
				</div>
			</div>
		</section>

	</main>

<//?php get_sidebar(); ?>

<?php get_footer(); ?>
