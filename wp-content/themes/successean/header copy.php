<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="site-wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

				<div class="top-bar">
					<div class="wrapper-94">
						<div class="top-bar-left">
							<h4>Fast Free Shipping Everyday on Orders over $50</h4>
						</div>
						<div class="top-bar-right">
							<ul>
								<li><a href="/my-account">My Account</a></li>
								<li><a href="/cart">Shopping Bag</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="menu-bar">
					<div class="wrapper-92">
						<div class="logo">
							<a href="<?php echo home_url(); ?>">
								<h1>Any Brand Shoes</h1>
							</a>
						</div>
					</div>
				</div>

				<!-- nav -->
				<nav class="nav" role="navigation">
					<?php html5blank_nav(); ?>
				</nav>
				<!-- /nav -->

<!-- 				 <img src="<?php echo get_template_directory_uri(); ?>/img/3-lines.png" alt="border-bottom" class="border-bottom"> -->
			</header>
			<!-- /header -->
