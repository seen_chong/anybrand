<?php get_header(); ?>

		<!-- section -->
		<section class="frontPageTopAlt">
			<div class="cartBanner">
				<div class="cartBannerHeader">
					<h2><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h2>
				</div>
			</div>
		</section>

		<div class="searchResults">
			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>
		</div>
		<!-- /section -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
