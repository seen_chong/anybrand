<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<section class="frontPageTopAlt">
	<div class="mainBanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner-4.jpg);">
		<div class="mainBannerText">
			<h1>shop</h1>
			<h1><span>woman</span></h1>
		</div>
	</div>
	<div class="headsUpBar">
		<p>get last season's best finds at even better prices</p>
	</div>
	<div class="filterBar" id="sticker1">

		<div class="filterWrapper" id="size">

			<div class="filterSizeOptions" id="sizeOption">
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('size-filter')) ?>
			</div>

			<div class="filterSizeOptions" id="widthOption">
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('width-filter')) ?>
			</div>

			<div class="filterSizeOptions" id="colorOption" >
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('color-filter')) ?>
			</div>
		</div>
	</div>
</section>



<section>
<!-- 	<div class="shopBy" style="margin-top:50px;margin-bottom:50px;width:100%;">

		<div class="selectLength catSelect">
			<div class="title">
				<h6>select length</h6>
			</div>
			<//?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('size-widget')) ?>

		</div>
		<div class="selectWidth catSelect">
			<div class="title">
				<h6>select width</h6>
			</div>
			<//?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('width-widget')) ?>
		</div>

		<div class="selectColor catSelect">
			<div class="title">
				<h6>select color</h6>
			</div>
			<//?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('color-widget')) ?>
		</div>

	</div> -->
</section>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<//?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>


<script type="text/javascript">
jQuery("li#showSize").hover(function(e){

    jQuery("div#sizeOption").toggle();

});

jQuery("li#showWidth").hover(function(e){

    jQuery("div#widthOption").toggle();

});

jQuery("li#showColor").hover(function(e){

    jQuery("div#colorOption").toggle();

});

</script>
