<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery.sticky.js"></script>
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
<body <?php body_class(); ?>>

	<!-- wrapper -->
	<div class="siteWrapper">
		

			<header class="header clear" role="banner">

				<nav class="nav sitePush" role="navigation">
					<div class="navWrapper">
					<div class="navLeft">
						<a href="/my-account">log in </a> <span id="gap">/</span> <a href="/my-account"> register</a>
					</div>
					<div class="navRight">
						<ul>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/my-bag.png">
								<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
							</li>
							<li>
								<?php echo do_shortcode("[wpbsearch]"); ?>
							</li>
						</ul>
					</div>
					</div>
				</nav>

			</header>
	<section class="frontPageTop sitePush">
		<div class="wrapper970" id="fixedWrapper">
			<div class="mainBanner" id="homeOnly" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner-1.jpg);">
				<div class="mainBannerSlider"> 
					<h5>Free Shipping</h5>
					<p>Get free shipping today<br> on your order over $50</p>
				</div>
				<div class="mainBannerText">
					<h1><a href="/about">about</a></h1>
					<h1><span><a href="/shop">woman</a></span></h1>
				</div>
				<a href="#scrollTo" class="pageDown">
					<img src="<?php echo get_template_directory_uri(); ?>/img/page-down.png">
				</a>
			</div>
		</div>

		<div class="navBottom sitePush" id="sticker">
			<div class="navWrapper">
			<div class="navBottomLogo">
				<a href="/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/anybrand-logo.png">
				</a>
			</div>
			<div class="navBottomLinks">
				<ul>
					<li>
						<a href="/shop">our story</a>
					</li>
					<li>
						<a href="/product-tag/clearance/">clearance</a>
					</li>
					<li>
						<a href="/shop">shop all</a>
					</li>
					<li>
						<a href="/contact">contact</a>
					</li>
				</ul>
			</div>
			</div>
		</div>
		<div class="wrapper970">
			<div class="newsletterBar" id="homeOnly">
				<div class="newsletterBarLeft">
					<p>get last season's best finds at even better prices</p>
				</div>
				<div class="newsletterBarRight">
					<?php echo do_shortcode('[contact-form-7 id="85" title="Newsletter Subscription"]'); ?>					
				</div>
			</div>
		</div>
	</section>
<div class="wrapper970 sitePush">