<?php get_header(); ?>

<section class="frontPageTopAlt">
	<div class="cartBanner">
		<div class="cartBannerHeader">
			<h2>Contact Us</h2>
		</div>
	</div>
		<div class="headsUpBar" style="height:65px;">
		<p>For customer service, information regarding outstanding orders, press requests and all other request please send a detailed message, including all necessary contact information to.</p>
	</div>

	<div class="contactUsForm">
		<?php echo do_shortcode('[contact-form-7 id="2194" title="Contact"]'); ?>
	</div>
</section>

</div> <!-- .wrapper970 -->

<section style="margin-top:40px;">

		<div class="wrapper970">
			<div class="midBannerBottom">
			<div class="midBannerHeader">
				<h2>what we're wearing</h2>
			</div>
			<div class="midBannerContent" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner-3.png);">
			</div>

		</div>
</section>

<?php get_footer(); ?>


<style type="text/css">

</style>