<?php get_header(); ?>

	<section class="shopSection" id="scrollTo">
		<div class="shopBy">
			<div class="header">
				<h6>shop by size</h6>
			</div>
			<div class="selectLength">
				<div class="title">
					<h6>select size</h6>
				</div>
				
				<ul>
					<a href="/shop?filter_sizes=5">
						<li>
							<p>5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=5-5">
						<li>
							<p>5.5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=6">
						<li>
							<p>6</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=6-5">
						<li>
							<p>6.5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=7">
						<li>
							<p>7</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=7-5">
						<li>
							<p>7.5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=8">
						<li>
							<p>8</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=8-5">
						<li>
							<p>8.5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=9">
						<li>
							<p>9</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=9-5">
						<li>
							<p>9.5</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=10">
						<li>
							<p>10</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=11">
						<li>
							<p>11</p>
						</li>
					</a>
					<a href="/shop?filter_sizes=12">
						<li>
							<p>12</p>
						</li>
					</a>
				</ul>
			</div>
			<div class="selectWidth">
				<div class="title">
					<h6>select width</h6>
				</div>
				<ul>
					<a href="/shop?filter_width=n">
						<li>
							<p>narrow</p>
						</li>
					</a>
					<a href="/shop?filter_width=m">
						<li>
							<p>medium</p>
						</li>
					</a>
					<a href="/shop?filter_width=w">
						<li>
							<p>wide</p>
						</li>
					</a>
					<a href="/shop?filter_width=ww">
						<li>
							<p>double-wide</p>
						</li>
					</a>
				</ul>
			</div>
			<a href="/shop"><button>Find Me Shoes</button></a>
		</div>
		<div class="browseBy">
			<div class="title">
				<h6>browse by look</h6>
				<p>explore our hand-curated collections,<br> broken down by session</p>
			</div>
			<div class="browseCold">
				<h6>fall / winter</h6>
				<div class="selection">
					<a href="/product-tag/fallwinter/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/selection-1.jpg">
					<p>exotic heels</p>
					</a>
				</div>
				<div class="selection">
					<a href="/product-tag/fallwinter/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/selection-2.jpg">
						<p>knee-high boots</p>
					</a>
				</div>
			</div>
			<div class="browseHot">
				<h6>spring / summer</h6>
				<div class="selection">
					<a href="/product-tag/springsummer/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/selection-3.jpg">
						<p>exotic heels</p>
					</a>
				</div>
				<div class="selection">
					<a href="/product-tag/springsummer/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/selection-4.jpg">
						<p>sneakers</p>
					</a>
				</div>
			</div>
		</div>
	</section>
</div> <!-- .wrapper970 -->

<section class="frontPageMid">
		<div class="midBannerTop" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner-2.jpg);">
			<div class="midBannerSlider"> 
				<h5>this week's featured sale</h5>
			</div>
			<div class="midBannerText">
				<h1>smile.<br> we have a 30% off <br> sale going on</h1>
			</div>
		</div>

		<div class="wrapper970">
			<div class="midBannerBottom">
			<div class="midBannerHeader">
				<h2><span>inspiration Nov 16</span><br>what we're wearing</h2>
			</div>
			<div class="midBannerContent" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner-3.png);">
				<div class="midBannerText">
					<h2>$69.95</h2> 
					<h6>Python-Stamped Danae Boots </h6>
					<a href="/shop">
						<p>Shop It Now </p>
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-white.png">
					</a>
				</div>
			</div>

		</div>
</section>

<section class="white">
<div class="wrapper970">
			<div class="productShowcase">
				<h4>Bella Vita</h4>
				<ul>
					<li>
						<a href="product/diza-camel/">
							<img src="/wp-content/uploads/2016/11/Diza-50-6214-Camel-Black-Gore-5-600x400.jpg">
							<h6>Diza Camel</h6>
							<p>$32.99</p>
						</a>
					</li>
					<li>
						<a href="/product/kiki-grey/">
							<img src="/wp-content/uploads/2016/11/Kiki-50-6457-Grey-5-600x400.jpg">
							<h6>Kiki Grey</h6>
							<p>$45.00</p>
						</a>
					</li>
					<li>
						<a href="/product/liset-ii-red-patent/">
							<img src="/wp-content/uploads/2016/11/Liset-II-50-5565-Red-Patent-4-600x400.jpg">
							<h6>Liset II Red Patent</h6>
							<p>$38.00</p>
						</a>
					</li>
					<li>
						<a href="/product/leona-cracked-gold/">
							<img src="/wp-content/uploads/2016/11/Leona-50-7329-Cracked-Gold-5-600x400.jpg">
							<h6>Leona Cracked Gold</h6>
							<p>$38.00</p>
						</a>
					</li>
				</ul>
				<div class="seeMoreBtn">
					<a href="/shop"><button>See More</button></a>
				</div>
			</div>
		</div>
</section>
<?php get_footer(); ?>


<style type="text/css">

</style>