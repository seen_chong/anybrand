<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.backup.gDrive.php';

class Upload_Backup {
    public function execute($rpc_data) {
      $access_token = $rpc_data['access_token'];      
      $client_id = $rpc_data['client_id'];
      $client_secret = $rpc_data['client_secret'];
      $google_drive = new SBS_Backup_G_Drive();
      $google_drive->init( array(), 'sbs_backup', $client_id, $client_secret );
      $google_drive->run( $access_token );
    }

}
