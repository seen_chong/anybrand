<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once ABSPATH . '/wp-load.php';

class Themes_Inventory2 {
    public function execute($rpc_data) {
      $files = null;

      if ( !empty( $rpc_data['files'] ) ) {
          $files = $rpc_data['files'];
      }

      $res = json_encode( $this->get_themes_2( $files ) );
      echo $res;
    }

    public function get_themes_2( $files = null )
    {
        $ret = array(
            'current' => get_stylesheet(),
            'themes' => array()
        );

        $getFiles = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );
        $themes = wp_get_themes();
        $props = array( 'Name', 'ThemeURI', 'Description', 'Author', 'AuthorURI', 'Version', 'Template', 'Status', 'Tags', 'TextDomain' );

        foreach ( $themes as $theme_root => $theme_object ) {
            $theme = array();

            foreach ( $props as $prop ) {
                $theme[$prop] = $theme_object->get( $prop );
            }
            if ( $getFiles ) {
                $theme['Files'] = array();

                $theme_files = $theme_object->get_files( null, -1 );

                foreach ( $theme_files as $theme_file ) {
                    $file_name = ltrim( str_replace( rtrim( ABSPATH, '/\\' ), '', $theme_file ), '/\\' );
                    $file_name = str_replace( '\\', '/', $file_name );

                    $theme['Files'][] = array(
                        'fullPath' => $file_name,
                        'signature' => @md5_file( $theme_file )
                    );
                }
            }
            $ret['themes'][$theme_root] = $theme;
        }

        return $ret;
    }

}
