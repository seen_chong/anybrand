<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/library/error.php';
require_once SABRES_PLUGIN_DIR . '/_inc/sbr_utils.php';
require_once SABRES_PLUGIN_DIR . '/library/io.php';
require_once SABRES_PLUGIN_DIR . '/library/net.php';
require_once SABRES_PLUGIN_DIR . '/library/zip.php';

class Update_Plugin {
    public function execute($rpc_data) {
      $filename = null;

      if ( isset( $rpc_data['fileName'] ) && $rpc_data['fileName'] != '' ) {
          $filename = $rpc_data['fileName'];
      }      

      $res = $this->do_update_plugin( $filename );
      echo $res;

    }

    public function do_update_plugin( $update_file_name )
    {
        if ( empty( $update_file_name ) ) {
            SBS_Error::throwError( 'Update filename cannot remain empty' );
        }

        //self::UPDATE_URL
        $update_plugin_url = SbrUtils::t( 'plugin_download_url' ) . '/' . $update_file_name;
        $update_full_path = SABRES_PLUGIN_DIR . '/temp/update/update.zip';
        $update_file_path = dirname( $update_full_path );
        $update_extract_path = $update_file_path . '/extract';

        SBS_IO::delete_folder( $update_file_path, SABRES_PLUGIN_DIR );

        if ( !is_dir( $update_file_path ) ) {
            @mkdir( $update_file_path, 0755, true );
        }

        if ( !is_dir( $update_file_path ) ) {
            SBS_Error::throwError( 'Update folder cannot be created due to insufficent permissions' );
        }

        SBS_Net::download_file( $update_plugin_url, $update_full_path, 'application/zip' );

        if ( !is_dir( $update_extract_path ) ) {
            @mkdir( $update_extract_path, 0755, true );
        }

        if ( !is_dir( $update_extract_path ) ) {
            SBS_Error::throwError( 'Update extract folder cannot be created due to insufficent permissions' );
        }

        SBS_Zip::extract_file( $update_full_path, $update_extract_path );

        SBS_IO::copy_folder( $update_extract_path, SABRES_PLUGIN_DIR . '/../' );

        SBS_IO::delete_folder( $update_file_path, SABRES_PLUGIN_DIR );
    }

}
