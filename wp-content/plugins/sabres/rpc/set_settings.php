<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';

class Set_Settings {
    public function execute($rpc_data) {
      if ( empty( $rpc_data['settings'] ) ) {
        SBS_Fail::byeArr(array( 'message'=>"RPC: set_settings missing settings parameter",
                              'code'=>400
                             ));
      }

      $res=$this->save(urldecode( $rpc_data['settings'] ));
      echo $res;

    }

    public function save($settings = null) {
      parse_str( $settings, $insettings );
      if ( empty( $insettings ) )
        SBS_Fail::byeArr(array( 'message'=>"RPC: set_settings can not load settings in ".var_export($settings,true),
                              'code'=>500
                             ));
        return SbrSettings::instance()->set_values( $insettings );
    }
}
