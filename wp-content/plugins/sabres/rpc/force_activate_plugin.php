<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/library/net.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
require_once SABRES_PLUGIN_DIR.'/_inc/server.php';

class Force_Activate_Plugin {
    public function execute($rpc_data) {
      $res=json_encode($this->activate_plugin_if_necessary());
      echo $res;
    }

    Public function activate_plugin_if_necessary() {
      $settings=SbrSettings::instance();
      if ( $settings->websiteSabresServerToken == '' || $settings->websiteSabresClientToken == '' ) {
            $server = SBS_Server::getInstance();
            return $server->call( 'activate-plugin-request', '', array(
                'hostName' => SBS_Net::remove_protocol( get_site_url() ),
                'token' => $settings->token,
                'symmetricEncryptionKey' => $settings->symmetricEncryptionKey,
                'verifyHashSalt' => $settings->verifyHashSalt,
                'rpcMethod' =>'GET'
            ) );
        }
    }

}
