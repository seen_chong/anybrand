<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/library/system.php';

class Get_System_Info {
    public function execute($rpc_data) {
      $info = SBS_System::get_info();
      $res = json_encode( $info );
      echo $res;
    }

}
