<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.firewall.php';
require_once ABSPATH.'/wp-load.php';

function init_sbs_firewall() {
    Sabres::$firewall = SBS_Firewall::getInstance();
    Sabres::$firewall->init( Sabres::$settings->get_settings('mod_firewall') );

    if ( !defined( 'SBS_RPC' ) ) {
        Sabres::$firewall->process_request();
    }
}

add_action( 'init', 'init_sbs_firewall', 1 );
