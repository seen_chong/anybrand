<link href="<?php echo plugins_url('styles/bootstrap.css', __DIR__); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('styles/style.css', __DIR__); ?>" rel="stylesheet">
<div class="sabres-piv-content">
  <div class="sabres-header">
    <div class="sabres-logo">
    </div>
  </div>
  <div >
    <h1>Pre Install CheckList</h1>
    <h3>Thank you for installing our plugin!</h3>
    <div id="piv-checklist">
    </div>
    <h4 id="be-patient" style="display:none">Please be patient while we test your wordpress installation and hosting enviorment are supported.  </h4>
    <h4 id="finish-install"><strong>To complete your installtion we need to test your enviorment settings and download some addional libraries.<strong></h4>
    <h4 id="retry-install" style="display:none"><strong>Plugin installation did not complete. Click to retry the installation<strong></h4>
    <h4 id="success-install" style="display:none"><strong>Plugin installation is now complete.<strong></h4>
    <button id="finish-install-btn" onclick="sabres.preInstall.finishInstall()"><strong>Finish Installation<strong></button>
  </div>
</div>

<script type="text/javascript">
var sabres=sabres || {};
sabres.preInstall={};


(function(window, document,preInstall){


  function CallbackChain(asyncStarter,callback) {
    this.callback=callback;
    this.asyncStarter=asyncStarter;
  }
  CallbackChain.prototype.next=function(asyncStarter,callback) {
    var result=new CallbackChain(asyncStarter,callback);
    this.nextChain=result;
    result.prevChain=this;
    return result;

  }
  CallbackChain.prototype.doFire=function() {

    var callback=this.callback;
    if (this.nextChain) {
      var prev=callback
      var nextChain=this.nextChain;
      callback=function(result) {
        prev(result);
        nextChain.doFire();
      }
    }
    this.asyncStarter(callback);
  }
  CallbackChain.prototype.fire=function() {
    var itt=this;
    while (itt.prevChain) {
      itt=itt.prevChain;

    }
     itt.doFire();
  }

  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }



  var checklist=document.getElementById("piv-checklist");
  var pathArray = window.location.href.split( '/' );
  var protocol = pathArray[0];
  var host = pathArray[2];
  var baseURL = protocol + '//' + host;
  var pluginPath=baseURL+'/wp-content/plugins/sabres';
  var ajaxPath = pluginPath+'/admin/pre_install.php';


  function addCheckListItemResult(checklistResult) {
    var para=null;
    var node=null;

    if (checklistResult.title) {
      para = document.createElement("p");
      node = document.createTextNode(checklistResult.title);
      para.appendChild(node);
      checklist.appendChild(para);
    }

    para = document.createElement("p");
    var img=document.createElement("img");
    if (checklistResult.error) {
      node = document.createTextNode("    "+checklistResult.error);
      img.src=pluginPath+'/admin/images/error-check.png';
    }
    else {
      var message=checklistResult.message;


      if (message instanceof Object)
        message=JSON.stringify(message);
      node = document.createTextNode("    "+message);
      img.src=pluginPath+'/admin/images/ok-check.png';


    }
    para.appendChild(img);
    para.appendChild(node);
    checklist.appendChild(para);
  }

  function doChecklistAjax(op,resultCallback) {
    var xmlhttp = new XMLHttpRequest();
    var params="op="+op;
    var url=ajaxPath+"?"+params;


    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        resultCallback(this);
      }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

  }
  function handlerAjaxResponse(op) {
    return function(response) {
      if (response.status==200) {
        var result=null;
        try {
          result = JSON.parse(response.responseText);
        } catch (e) {
          result= {
            error : response.responseText
          }
        }
        addCheckListItemResult(result);
      }
      else {
        console.error("doChecklistAjax failed: op = "+op+", "+"status"+" "+response.status);
      }
    }
  }



  preInstall.finishInstall=function() {
    while (checklist.firstChild) {
      checklist.removeChild(checklist.firstChild);
    }
    var bePatient=document.getElementById("be-patient");
    bePatient.style.display="block";
    var finishInstall=document.getElementById("finish-install");
    finishInstall.style.display="none";
    var finishInstallBTN=document.getElementById("finish-install-btn");
    finishInstallBTN.style.display="none";
    var retryInstall=document.getElementById("retry-install");
    retryInstall.style.display="none";

    (new CallbackChain(function(callback) {
      doChecklistAjax("php-ver",callback);
    },handlerAjaxResponse("php-ver"))).next(
      function(callback) {
        doChecklistAjax("openssl",callback);
      },handlerAjaxResponse("openssl")).next(
        function(callback) {
          doChecklistAjax("webserver-type",callback);
        },handlerAjaxResponse("webserver-type")).next(
      function(callback) {
        doChecklistAjax("vendor-download",callback);
      },handlerAjaxResponse("vendor-download")).next(
      function(callback) {
        doChecklistAjax("vendor-extract",callback);
      },handlerAjaxResponse("vendor-extract")).next(
      function(callback) {
        doChecklistAjax("finalize",callback);
      },function(response) {
        if (response.status!=200) {
          console.error("Finalize failed: status "+response.status);
          return;
        }
        var result=null;
        try {
          result = JSON.parse(response.responseText);
        } catch (e) {
          result= {
            error : response.responseText,
            success : false
          }
        }
        if (result.error)
          console.error(result.error);
        if (!result.success) {
          retryInstall.style.display="block";
          finishInstallBTN.style.display="block";
        }
        else {
          var successInstall=document.getElementById("success-install");
          successInstall.style.display="block";
          setTimeout(function() {
            location.reload();
          }, 3000);
        }
        bePatient.style.display="none";

      })
    .fire();

  }

  if (getParameterByName('go')=='true')
    preInstall.finishInstall();



})(window, document,sabres.preInstall);
</script>
