<?php

if (defined('PRE_INSTALL_INC')) {

  if (!defined('SBS_MAIN_PLUGIN_FILE'))
    throw new Exception("SBS_MAIN_PLUGIN_FILE is undefined", 1);
    if (!defined('SABRES_PLUGIN_DIR'))
     throw new Exception("SABRES_PLUGIN_DIR is undefined", 1);

  class Pre_Install_WP_HOOKS {

    public function activation_hook() {
      $this->activate_pre_install_checklist_page();
    }

    public function register_admin_menu() {

      $customer=$downloadPath=SbrUtils::trgx('name');

      add_menu_page($customer.' - Pre Install Checklist', //page_title
        $customer, //menu title
        'activate_plugins', //capability
        $customer, //menue slug
        array( $this, 'show_pre_install_checklist' ), //function
        plugins_url() . '/sabres/admin/images/logo_dark_16x16.png'
      );
    }



    private function activate_pre_install_checklist_page() {
      set_transient( 'sabres_pre_install_checklist_activated', 1, 30 );
    }

    public function redirect_to_pre_install_checklist_page() {
          // only do this if the user can activate plugins
      if ( ! current_user_can( 'activate_plugins' ) )
      return;

      // don't do anything if the transient isn't set
      if ( ! get_transient( 'sabres_pre_install_checklist_activated' ) )
      return;

      delete_transient( 'sabres_pre_install_checklist_activated' );
      $customer=SbrUtils::trgx('name');
      wp_safe_redirect( admin_url( 'admin.php?page='.$customer.'&go=true'));
      exit;
    }

    public function show_pre_install_checklist() {
      //echo '<div class="wrap"><h2>Welcome to My Awesome Plugin</h2></div>';
      require SABRES_PLUGIN_DIR.'/admin/views/pre_install_view.php';
    }

  }


  $preInstall=new Pre_Install_WP_HOOKS();
  register_activation_hook( SBS_MAIN_PLUGIN_FILE, array( $preInstall, 'activation_hook' ) );
  add_action( 'admin_menu',  array( $preInstall, 'register_admin_menu' ) );
  add_action( 'admin_init', array( $preInstall, 'redirect_to_pre_install_checklist_page' ), 1 );

}
else {
    $base_path = preg_replace( '/wp-content(?!.*wp-content).*/', '', __DIR__ );
    require_once $base_path . 'wp-load.php';

    $user = wp_get_current_user();

    if (!isset($user) || !isset($user->roles) || !in_array('administrator',$user->roles)) {
      header('HTTP/1.0 403 Forbidden');
      return;
    }

    //Used as ajax controller for pre install view
    if (!isset($_GET) || !isset($_GET["op"])) {
      header('HTTP/1.1 500 Internal Server Error');
      return;
    }




    class Pre_Install_Tests {

      public function dispatch($op) {
        //Common Validation
        $result=array();
        if (!defined('SABRES_PLUGIN_DIR'))
           $result['error']="SABRES_PLUGIN_DIR is not defined";
        if (isset($result['error']))
          return $result;
        switch ($op) {
          case 'php-ver':
            return $this->testPHPVersion();
          case 'webserver-type':
          return $this->testWebServer();
          case 'openssl':
          return $this->testOpenSSL();
          case 'vendor-download' :
          return $this->testVendorDownload();
          case 'vendor-extract':
          return $this->testVendorExtract();
          case 'finalize':
          return $this->finalize();
          default:
          return $this->defaultCase();
        }
      }
      public function finalize() {
        $result=$this->dispatch('php-ver');
        if (isset($result['error']))
           return array('success'=>false);
        $result=$this->dispatch('webserver-type');
        if (isset($result['error']))
           return array('success'=>false);
        $result=$this->dispatch('openssl');
           if (isset($result['error']))
              return array('success'=>false);
        $downloadVendorsFile=SABRES_PLUGIN_DIR.'/downloadVendors';
        if (file_exists($downloadVendorsFile))
          return array('success'=>false);
        SbrSettings::instance()->preInstall="False";
        return array('success'=>true);
      }


      public function testPHPVersion() {
          $result=array();
          $result['title']="Checking PHP version (required 5.4 and above)";
          if ( version_compare( PHP_VERSION, '5.4', '<' ) )
              $result['error']="Failed your version is : ".phpversion();
          else
              $result['message']="Passed your version is : ".phpversion();
          return $result;

      }
      public function testOpenSSL() {
          $result=array();
          $result['title']="Checking open SSL Extension";
          if ( !extension_loaded('openssl') )
              $result['error']="Failed open SSL extension is not loaded";
          else
              $result['message']="Passed open SSL extension is loaded";
          return $result;

      }
      public function testWebServer() {
          $result=array();
          $result['title']="Checking hosting web server";
          $phpInfoJson=strtoupper(json_encode(SbrUtils::phpinfo_array(true)));
          $apacheOccurs=substr_count($phpInfoJson, strtoupper('apache'));
          $iisOccurs=substr_count($phpInfoJson, strtoupper('iis'));
          $nginxOccurs=substr_count($phpInfoJson, strtoupper('nginx'));
          if ($apacheOccurs==0 && $iisOccurs==0 && $nginxOccurs) {
            $result['message']="Uknown web server";
          }
          else {
            $webserver=null;
            $max=max($apacheOccurs,$iisOccurs,$nginxOccurs);
            if ($max==$apacheOccurs)
              $webserver='Apache';
            elseif ($max==$nginxOccurs)
              $webserver='Nginx';
            else
              $webserver='IIS';
            $result['message']=$webserver.' web server detected';
          }
          return $result;

      }

      private function optionNameVendorsDownloaded() {
        return 'Sabres_VendorsDownloaded';
      }

      private function downloadVendorArchive(&$result) {
        if (!defined('SBS_MAIN_PLUGIN_FILE')) {
           $result['error']="SBS_MAIN_PLUGIN_FILE is not defined";
           return;


        }
        $pluginData=get_plugin_data(SBS_MAIN_PLUGIN_FILE);
        if (!isset($pluginData)) {
          $result['error']="Could not find plugin data";
          return;
        }
        if (!isset($pluginData)) {
          $result['error']="Could not find plugin data";
          return;
        }
        if (!isset($pluginData['Version'])) {
          $result['error']="Could not find plugin version";
          return;
        }
        $downloadPath=SbrUtils::trgx('plugin_download_url');
        $vendorsZip=$downloadPath."/vendors-".$pluginData['Version'].".zip";
        require_once ABSPATH . 'wp-admin/includes/file.php';
        $download=download_url($vendorsZip);
        if (is_wp_error($download)) {
          $result['error']="Failed to download ".$vendorsZip.". ".$download->get_error_message().' '.$download->get_error_data();
          return;
        }

        return array('zipURL'=>$vendorsZip,'downloadPath'=>$download);
      }


      public function testVendorDownload() {
        $result=array();
        $result['title']="Checking vendor libs downloaded successfully";


        $vendorsDownloaded=get_option($this->optionNameVendorsDownloaded(),null);
        if (!isset($vendorsDownloaded)) {
            $downloadData=$this->downloadVendorArchive($result);
            if (isset($downloadData)) {
              add_option($this->optionNameVendorsDownloaded(),$downloadData['downloadPath']);
              $result['message']='Successfully downloaded vendors zip: '.$downloadData['zipURL'];
            }
        }
        else {
          $result['message']="Vendor libs already downloaded successfully";
        }
        return $result;
      }

      public function testVendorExtract() {
        $result=array();
        $result['title']="Extracting vendor libs zip file";
        $vendorsDownloadFile=get_option($this->optionNameVendorsDownloaded(),null);
        if (!isset($vendorsDownloadFile))
            $result['error']='Venodr libs file was not downloaded';
        else  {
          global $wp_filesystem;
          // Initialize the WP filesystem, no more using 'file-put-contents' function
          if (empty($wp_filesystem)) {
              require_once (ABSPATH . '/wp-admin/includes/file.php');
              WP_Filesystem();
          }
          $destination=SABRES_PLUGIN_DIR.'/library';
          $unzipResult=unzip_file($vendorsDownloadFile,$destination);
          if (is_wp_error($unzipResult)) {
            $result['error']="Failed to extract vendor libs. ".$unzipResult->get_error_message().' '.$unzipResult->get_error_data();
          }
          else {
            unlink($vendorsDownloadFile);
            $downloadVendorsFile=SABRES_PLUGIN_DIR.'/downloadVendors';
            if (file_exists($downloadVendorsFile))
               unlink($downloadVendorsFile);
            delete_option($this->optionNameVendorsDownloaded());
            $result['message']="Successfully extracted vendor libs";
          }
        }
        return $result;
      }


      public function defaultCase() {
          $result=array();
          header('HTTP/1.1 500 Internal Server Error');
          $result['error']='Invalid op '.$op;
          return $result;

      }

    }






    $op=$_GET["op"];
    $tests=new Pre_Install_Tests();
    $result=$tests->dispatch($op);
    echo json_encode($result);
}
