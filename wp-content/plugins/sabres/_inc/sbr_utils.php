<?php
///
/// Created: 12/01/2016
/// By: Ronny Sherer
/// Purpose: General purpose utilities of the plugin.
///
class SbrUtils
{
    protected static $user_meta_key_prefix = 'sbr_';

    static public function associated_array_to_obj( $arr )
    {
        $obj = (object)$arr;
        foreach ( $arr as &$value ) {
            if ( is_array( $value ) )
                $value = (object)$value;
        }
        return $obj;
    }

    static public function obj_to_associated_array( $obj )
    {
        $arr = (array)$obj;
        foreach ( $arr as &$value ) {
            if ( is_object( $value ) )
                $value = (array)$value;
        }
        return $arr;
    }

    static public function get_json( $arr )
    {
        return json_encode( self::associated_array_to_obj( $arr ) );
    }

    static public function set_debug( $is_debug )
    {
        self::$debug = $is_debug;
    }

    static public function debug_log( $str )
    {
        file_put_contents( dirname( dirname( __FILE__ ) ) . '/debug.log', date( "Y-m-d H:i:s" ) . " - $str\n", FILE_APPEND );
    }

    static public function is_true_of_false($string,&$result) {
      $result=null;
      if (gettype($string)!='string')
        return false;
      if (strtolower($string)=='true') {
        $result=true;
        return true;
      }
      if (strtolower($string)=='false') {
        $result=false;
        return true;
      }
      return false;

    }

    public static function is_integer($string,&$result) {
      $result=null;
      if (!is_numeric($string))
        return false;
      $intVal=intVal($string);
      if ($intVal!=floatVal($string))
        return false;
      $result=$intVal;
      return true;

    }

    static public function update_user_meta($user_id, $meta_key, $meta_value, $prev_value = '') {
        return update_user_meta( $user_id, static::$user_meta_key_prefix . $meta_key, $meta_value, $prev_value );
    }

    static public function get_user_meta($user_id, $key, $single = false) {
        return get_user_meta( $user_id, static::$user_meta_key_prefix . $key, $single );
    }

    static private $debug = false;

    private static $conf_instance;

    //gets the value for key $name from conf.yaml using spyc lib
    public static function t( $name, $group = '' )
    {
        if ( !self::$conf_instance ) {
            if (!self::load_conf()) {
                return NULL;
            }
        }
        if ( $group && isset( self::$conf_instance[$group] ) && isset( self::$conf_instance[$group][$name] ) ) {
            return self::$conf_instance[$group][$name];
        } elseif ( isset( self::$conf_instance[$name] ) ) {
            return self::$conf_instance[$name];
        }
        return NULL;
    }

    protected static function load_conf()
    {
        $filename = dirname( dirname( __FILE__ ) ) . '/conf/conf.yaml';

        if ( !function_exists( 'spyc_load_file' ) ) {
            require_once SABRES_PLUGIN_DIR . '/library/vendor/autoload.php';
            if ( !function_exists( 'spyc_load_file' ) ) {
              return false;
            }
        }

        self::$conf_instance = spyc_load_file( $filename );

        return true;
    }

    private static $conf_string_instance;

    //gets the value for key $name from conf.yaml using regex. Good when library is not available
    public static function trgx($name) {
      if (!self::$conf_string_instance) {
          $confPath=SABRES_PLUGIN_DIR.'/conf/conf.yaml';
          if (!file_exists($confPath)) {
            throw new Exception("Configuration file not found: ".$confPath, 1);
          }
          self::$conf_string_instance=file_get_contents($confPath);
          if (self::$conf_string_instance===False) {
            throw new Exception("Failed to read conf file: ".$confPath , 1);
          }
      }
      $matches=array();
      if (1!==preg_match('/'.preg_quote($name,'/').'\s*:\s*(\S+)/m',self::$conf_string_instance,$matches)) {
        return null;
      }
      return $matches[1];
    }


    public static function get_mail_err() {
      global $ts_mail_errors;
      global $phpmailer;
      if (!isset($ts_mail_errors)) $ts_mail_errors = array();
      if (isset($phpmailer)) {
        $ts_mail_errors[] = $phpmailer->ErrorInfo;
      }
      return print_r($ts_mail_errors,true);
   }
   public static function  phpinfo_array($return=false){
      /* Andale!  Andale!  Yee-Hah! */
      ob_start();
      phpinfo(-1);

      $pi = preg_replace(
      array('#^.*<body>(.*)</body>.*$#ms', '#<h2>PHP License</h2>.*$#ms',
      '#<h1>Configuration</h1>#',  "#\r?\n#", "#</(h1|h2|h3|tr)>#", '# +<#',
      "#[ \t]+#", '#&nbsp;#', '#  +#', '# class=".*?"#', '%&#039;%',
        '#<tr>(?:.*?)" src="(?:.*?)=(.*?)" alt="PHP Logo" /></a>'
        .'<h1>PHP Version (.*?)</h1>(?:\n+?)</td></tr>#',
        '#<h1><a href="(?:.*?)\?=(.*?)">PHP Credits</a></h1>#',
        '#<tr>(?:.*?)" src="(?:.*?)=(.*?)"(?:.*?)Zend Engine (.*?),(?:.*?)</tr>#',
        "# +#", '#<tr>#', '#</tr>#'),
      array('$1', '', '', '', '</$1>' . "\n", '<', ' ', ' ', ' ', '', ' ',
        '<h2>PHP Configuration</h2>'."\n".'<tr><td>PHP Version</td><td>$2</td></tr>'.
        "\n".'<tr><td>PHP Egg</td><td>$1</td></tr>',
        '<tr><td>PHP Credits Egg</td><td>$1</td></tr>',
        '<tr><td>Zend Engine</td><td>$2</td></tr>' . "\n" .
        '<tr><td>Zend Egg</td><td>$1</td></tr>', ' ', '%S%', '%E%'),
      ob_get_clean());

      $sections = explode('<h2>', strip_tags($pi, '<h2><th><td>'));
      unset($sections[0]);

      $pi = array();
      foreach($sections as $section){
         $n = substr($section, 0, strpos($section, '</h2>'));
         preg_match_all(
         '#%S%(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?%E%#',
           $section, $askapache, PREG_SET_ORDER);
         foreach($askapache as $m)
             $pi[$n][$m[1]]=(!isset($m[3])||$m[2]==$m[3])?$m[2]:array_slice($m,2);
      }

      return ($return === false) ? print_r($pi) : $pi;
    }

    public static function get_files( $path, $recursive = null )
    {
        $ret = array();

        $folders = glob( realpath( $path ) . "/*", GLOB_ONLYDIR );
        $files = glob( realpath( $path ) . "/*", GLOB_BRACE );

        foreach ( $files as $file ) {
            $ret[] = $file;
        }

        if ( $recursive ) {
            foreach ( $folders as $folder ) {
                $folder_files = self::get_files( $folder, $recursive );
                $ret = array_merge( $ret, $folder_files );
            }
        }

        return $ret;
    }

    private static $utc=null;
    public static function nowUTC() {
      if (self::$utc==null)
        self::$utc=new DateTimeZone('UTC');
      return new DateTime('now',self::$utc);
    }
}
