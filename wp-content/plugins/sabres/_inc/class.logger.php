<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Logger' ) ) {
     require_once SABRES_PLUGIN_DIR.'/_inc/class.singleton.php';

    /**
     * The Sabres Logger Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Logger extends SBS_Singleton
    {
        private $settings;

        const LOG_TABLE_NAME = 'sbs_log';

        protected static $instance;

        protected function __construct()
        {
        }

        public function init( $settings = null )
        {
            $this->settings = $settings;
        }

        public function log( $type, $logger, $message, $data = null )
        {
            require_once ABSPATH.'/wp-load.php';
            global $wpdb;

            $table_name = $wpdb->prefix . self::LOG_TABLE_NAME;

            $sql = "INSERT INTO $table_name ( $table_name.log_type, $table_name.logger, $table_name.message, $table_name.log_data) VALUES (%s, %s, %s, %s)";

            return $wpdb->query( $wpdb->prepare( $sql, $type, $logger, $message, ( isset( $data ) ? json_encode( $data, JSON_FORCE_OBJECT ) : null ) ) );
        }

        public function get_entries( $start = null, $end = null )
        {
            require_once ABSPATH.'/wp-load.php';
            global $wpdb;

            $table_name = $wpdb->prefix . self::LOG_TABLE_NAME;

            $sql = "SELECT * FROM $table_name";

            if ( !empty( $start ) && !empty( $end ) ) {
                $sql .= " WHERE $table_name.created_at >= '%s' AND $table_name.created_at < '%s'";

                $res = $wpdb->get_results( $wpdb->prepare( $sql, $start, $end ), OBJECT );
            } else {
                $res = $wpdb->get_results( $sql, OBJECT );
            }

            if ( !empty( $res ) ) {
                foreach ( $res as &$entry ) {
                    if ( !empty( $entry->log_data ) ) {
                        $entry->log_data = json_decode( $entry->log_data );
                    }
                }
            }

            return $res;
        }

        public function clear_entries( $start = null, $end = null )
        {
            require_once ABSPATH.'/wp-load.php';
            global $wpdb;

            $table_name = $wpdb->prefix . self::LOG_TABLE_NAME;

            $sql = "DELETE FROM $table_name";
            $criteria = '';

            if ( !empty( $start ) ) {
                $criteria .= " WHERE $table_name.created_at >= '%s'";

                $query = $wpdb->prepare( $sql . $criteria, $start );
            }

            if ( !empty( $end ) ) {
                if ( empty( $criteria ) ) {
                    $criteria .= ' WHERE ';
                } else {
                    $criteria .= ' AND ';
                }
                $criteria .= " $table_name.created_at < '%s'";

                $query = $wpdb->prepare( $sql . $criteria, $end );
            }

            return $wpdb->query( $query );
        }
    }
}
