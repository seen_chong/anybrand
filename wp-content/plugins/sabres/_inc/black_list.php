<?php
///
/// Created: 07/01/2016
/// By: Ronny Sherer
/// Purpose: Blocking items class. It blocks IPs and cookies.
///          Black list is saved as an array in PHP file named blocking_array.php
///          and in the database on option table as a json in key 'sbr-blockings'.
///          If the file does not exists, than the database option is read.
///          On init, method is_blocked() is called twice. With theIP and with the
///          uniqueID cookie.
///          Method block() is called when endpoint is accesses with op=block;
///
///

class BlackList
{
	/////////////////////////////////////////
	// Public:

	public function __construct()
	{
		$this->blockingFile = SABRES_PLUGIN_DIR.'/'.$this->blockingFile;
		$this->load_list();
	}

	public function is_blocked($key)
	{
		$key = str_replace('.', '_', $key);		// parse_str replaces '.' by '_'
		return array_key_exists($key, $this->blockedItems) ? ($this->blockedItems[$key] > time()) : false;
	}

	public function block($elements, $reset = false)
	{
		if ($reset)
			$this->blockedItems = array();

		// Not array if reset with no blocking
		if (is_array($elements))
		{
			foreach ($elements as $elem => $time)
				$this->blockedItems[$elem] = time() + 60 * $time;
		}
		$this->store();
		return $this->get_json();
	}

	/////////////////////////////////////////
	// Private:

	private function load_list()
	{
		if (file_exists($this->blockingFile))
		{
			require $this->blockingFile;	// Loads array
			$this->blockedItems = $sbr_blockedItems;
		}
		else
		{
			$this->load_from_database();
		}
	}

	private function store()
	{
		$this->remove_expired();
		if (empty($this->blockedItems))
		{
			if (file_exists($this->blockingFile))
			{
				unlink($this->blockingFile);
			}
		}
		else
		{
			$str = "<?php\n\$sbr_blockedItems = array(";
			foreach ($this->blockedItems as $key => $time)
			{
				$str .= "\n\t'$key' => $time,";
			}
			$str .= "\n);";
			file_put_contents($this->blockingFile, $str);
		}
		$this->save_to_database();
	}

	private function remove_expired()
	{
		$remove = array();
		$now = time();
		foreach ($this->blockedItems as $key => $time)
		{
			if ($now > $time)
				$remove[] = $key;
		}
		foreach ($remove as $key)
			unset($this->blockedItems[$key]);
	}

	private function load_from_database()
	{
		$wp_sbr_blockings = get_option($this->blockingsOption);
		if (empty($wp_sbr_blockings))
			return false;

		$sbr_blockedItems = SbrUtils::obj_to_associated_array( json_decode($wp_sbr_blockings) );
		if (!empty($sbr_blockedItems))
		{
			$this->blockedItems = array_merge($this->blockedItems, $sbr_blockedItems);
			$this->store();
		}

		return true;
	}

	private function save_to_database()
	{
		$json = $this->get_json();
		update_option($this->blockingsOption, $json );
	}

	/// Get the settings as a JSON string
	private function get_json()
	{
		return SbrUtils::get_json($this->blockedItems);
	}

	private $blockingsOption = 'sbr-blockings';
	private $blockedItems = array();
	private $blockingFile = 'blocking_array.php';
}
