<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php if ( ! $product->is_sold_individually() ) : ?>
		<?php woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) ); ?>
	<?php endif; ?>
	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
<div class="shareProduct">
	<ul>
		<li>
			<a href="">
				<img src="<?php echo get_template_directory_uri(); ?>/img/social-group.png">
			</a>
		</li>
	</ul>
</div>
<div class="productDetails">
	<div class="productDetailBlock">
		<h6>Product details</h6>
		<ul>
			<li>Shiny goatskin</li>
			<li>Pointed toe</li>
			<li>'Broken' heel effect</li>
			<li>Tone-on-tone covered heel</li>
			<li>Interior zip</li>
			<li>110 mm arch</li>
			<li>Made in Italy</li>
		</ul>
	</div>
	<div class="productDetailBlock">
		<h6>shipping returns</h6>
	</div>
	<div class="productDetailBlock">
		<h6>about the brand</h6>
	</div>
</div>
